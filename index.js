const express = require('express');
const controllers = require('./routes/routes');
const PORT = 8080;

const app = express();

app.use(express.json());
app.use('/api/files', controllers);

app.listen(PORT, () => console.log('Server is ready'));