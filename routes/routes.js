const express = require('express');
const router = express.Router();
const {
	createFile,
	getFiles,
	getFile
} = require('../controllers/controllers');

router.route('/').get(getFiles).post(createFile);
router.route('/:filename').get(getFile);

module.exports = router;