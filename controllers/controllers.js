const fs = require('fs');

const createFile = (req, res) => {
	try {
		const extensions = /.(log|txt|json|yaml|xml|js)$/g;
		const data = req.body;
		const filename = 'filename' in data ? data.filename : false;
		const content = 'content' in data ? data.content : false;
		
		if(content && filename) {
			if(filename.match(extensions)) {
				fs.writeFile(`./files/${filename}`, content, err => {
					if(err) {
						throw err;
					} else {
						res.status(200).json({ message: "File created successfully" });
					}
				});
			} else {
				res.status(500).json({ "message": "Server error" });
				throw `Invalid extension of file - .${filename.split('.').pop()}`
			}
		} else if(!content && filename) {
				res.status(400).json({ "message": "Please specify 'content' parameter" });
				throw `Content parameter doesn't exist`;
		} else if(!filename && content) {
				res.status(500).json({ "message": "Server error" });
				throw `Filename parameter doesn't exist`;
		} else {
				res.status(500).json({ "message": "Server error" });
				throw `Parameters 'filename' and 'content' don't exist`;
		}
		
	} catch (err) {
		logErrors(err);
	}
}

const getFiles = (req, res) => {
	try {
		const fileNames = [];
		
		fs.access('./files/', (err) => {
			
			if(err) {
				logErrors(err);
			} else {
				fs.readdir('./files/', (err, files) => {
					if(err) {
						throw err;
					} else {
						files.forEach(file => {
							fileNames.push(file);
						});
						
						if(fileNames.length) {
							res.status(200).json({
								"message": "Success",
								"files": fileNames
							});
						} else {
							res.status(500).json({ "message": "Server error" });
						}
					}
				})
			}
		})
	
	} catch (err) {
		logErrors(err);
	}
}

const getFile = (req, res) => {
	try {
		const filename = req.params.filename;
		const path = `./files/${filename}`;
		
		fs.access(path, (err) => {
			if(err) {
				logErrors(err);
				res.status(400).json({ "message": `No file with ${filename} filename found` });
			} else {
				let uploadedDate = '';
				
				fs.readFile(path, 'utf8', (err, data) => {
					if(err) {
						logErrors(err);
					} else {
						fs.stat(path, (err, stats) => {
							if(err) {
								logErrors(err);
							} else {
								uploadedDate = stats.birthtime;
								
								res.status(200).json({
									"message": "Success",
									"filename": `${filename}`,
									"content": `${data}`,
									"extension": `${filename.split('.').pop()}`,
									"uploadedDate": `${uploadedDate}`
								})
							}
						})
					}
				})
			}
		})
	} catch (err) {
		logErrors(err);
		res.status(500).json({ "message": "Server error" });
	}
}

const logErrors = (err) => {
	const log = new Date() + ' : ' + err.toString() + '\n';
	fs.appendFile('logErrors.txt', log, (err) => {
		if(err) {
			throw err;
		}
	});
}

module.exports = {
	createFile,
	getFiles,
	getFile
}